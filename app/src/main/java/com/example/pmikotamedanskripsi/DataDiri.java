package com.example.pmikotamedanskripsi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class DataDiri extends AppCompatActivity {

    EditText namaa, tgllahir;
    AutoCompleteTextView showw;
    ImageView show1;
String nohpp, selection1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_diri);
        nohpp = FirebaseAuth.getInstance().getCurrentUser().getPhoneNumber();
        namaa = findViewById(R.id.NAMA);
        showw = findViewById(R.id.acomponen);
        show1 = findViewById(R.id.acomponen2);
        tgllahir = findViewById(R.id.TGL);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(DataDiri.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.goldarah));
        showw.setAdapter(arrayAdapter);
        showw.setCursorVisible(false);
        showw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                showw.showDropDown();

            }
        });
        showw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showw.showDropDown();
                selection1=(String) adapterView.getItemAtPosition(i);


            }
        });
        show1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showw.showDropDown();
            }
        });
        tgllahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(DataDiri.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        tgllahir.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });


        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Pengguna").child(nohpp);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                String a = dataSnapshot.child("Nama").getValue().toString();
                String b = dataSnapshot.child("Goldarah").getValue().toString();
                String c = dataSnapshot.child("Tanggal").getValue().toString();
                namaa.setHint(a);
                showw.setText(b);
                tgllahir.setHint(c);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        findViewById(R.id.update).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = namaa.getText().toString();
                String tanggal = tgllahir.getText().toString();
                String GOL = showw.getText().toString();
                if(name.isEmpty()){
                    Toast.makeText(DataDiri.this,"Masukan Nama",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(tanggal.isEmpty()){
                    Toast.makeText(DataDiri.this, "Masukan Tanggal Lahir",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (GOL.isEmpty()) {
                    Toast.makeText(DataDiri.this, "Pilih Golngan Darah", Toast.LENGTH_SHORT).show();
                    return;
                }

                else {
                    simpan(name,tanggal,GOL);
                }


            }
        });
        findViewById(R.id.keluar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(DataDiri.this, MainActivity.class));
                finish();
            }
        });
    }
    private void simpan (final String nama, String tanggal, String darah){

       DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Pengguna").child(nohpp);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Nama", nama );
        hashMap.put("Tanggal", tanggal);
        hashMap.put("Goldarah",darah);
        hashMap.put("No",nohpp);



        reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                namaa.setText("");
                tgllahir.setText("");


            }
        });
}}