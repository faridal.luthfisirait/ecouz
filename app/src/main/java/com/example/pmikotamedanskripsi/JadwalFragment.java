package com.example.pmikotamedanskripsi;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class JadwalFragment extends Fragment {
    RecyclerView rec;
    private int CAMREA_CODE = 1;
    FloatingActionButton floatingActionButton;
    ArrayList<DataFirebaseJadwal> arrayList;
    FirebaseRecyclerOptions<DataFirebaseJadwal> options;
    FirebaseRecyclerAdapter<DataFirebaseJadwal,DataViewHolder > firebaseRecyclerAdapter;


    public JadwalFragment() {
        // Required empty public constructor
    }
    @Override
    public void onStart() {
        firebaseRecyclerAdapter.startListening();


        super.onStart();
    }

    @Override
    public void onStop() {
        firebaseRecyclerAdapter.stopListening();
        super.onStop();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_jadwal, container, false);
        rec = rootView.findViewById(R.id.recc);
        rec.setHasFixedSize(true);
        requestPermission();
        rec.setLayoutManager( new LinearLayoutManager(getActivity()));
        arrayList =  new ArrayList<DataFirebaseJadwal>();
        final DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Jadwal");
        reference.keepSynced(true);
        options =  new FirebaseRecyclerOptions.Builder<DataFirebaseJadwal>().setQuery(reference,DataFirebaseJadwal.class).build();
        firebaseRecyclerAdapter =  new FirebaseRecyclerAdapter<DataFirebaseJadwal, DataViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final DataViewHolder dataViewHolder, int i, @NonNull final DataFirebaseJadwal dataFirebaseJadwal) {


                dataViewHolder.Waktu.setText(dataFirebaseJadwal.getWaktu());
                dataViewHolder.Tanggal.setText(dataFirebaseJadwal.getTanggal());
                dataViewHolder.Alamat.setText(dataFirebaseJadwal.getAlamat());
                dataViewHolder.Tempat.setText(dataFirebaseJadwal.getTempat());
                String satu = dataViewHolder.Tanggal.getText().toString();
                String dua = dataViewHolder.Tempat.getText().toString();
                final String tiga = FirebaseAuth.getInstance().getUid();
                final DatabaseReference reference1 = FirebaseDatabase.getInstance().getReference("Jadwal").child(satu + dua);
                reference1.keepSynced(true);
                reference1.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String a = (String) dataSnapshot.child("Respon").child(tiga).child("jumlahh").getValue();
                        if (a == null) {
                            dataViewHolder.love.setActivated(true);
                            dataViewHolder.love.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.ic_favorite_black_24dp));

                        } else {
                            dataViewHolder.love.setActivated(false);
                        dataViewHolder.love.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.ic_favorite_red_24dp));
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                dataViewHolder.love.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String uid = FirebaseAuth.getInstance().getUid();
                        String satu = dataViewHolder.Tanggal.getText().toString();
                        String dua = dataViewHolder.Tempat.getText().toString();
                        if (dataViewHolder.love.isActivated()) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Perhatiam");
                            builder.setMessage("Apakah Anda benar ingin Melakukan donor darah pada tanggal "+satu+" di" + dua + "? kami akan buat alarm");
                            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    String uid = FirebaseAuth.getInstance().getUid();
                                    String satu = dataViewHolder.Tanggal.getText().toString();
                                    String dua = dataViewHolder.Tempat.getText().toString();
                                    String tiga = dataViewHolder.Waktu.getText().toString();
                                    String empat = dataViewHolder.Alamat.getText().toString();
                                    DatabaseReference r = FirebaseDatabase.getInstance().getReference("Jadwal").child(satu + dua);
                                    r.child("Respon").child(uid).child("jumlahh").setValue("1");
                                    dataViewHolder.love.setActivated(true);
                                    dataViewHolder.love.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.ic_favorite_red_24dp));
                                    pushAppointmentsToCalender(getActivity(),satu,dua, tiga,empat);



                                }
                            });
                            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();


                        } else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setTitle("Perhatiam");
                            builder.setMessage("Apakah Anda benar ingin Membatalkan donor darah pada tanggal "+satu+" di " + dua + "?");
                            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    String uid = FirebaseAuth.getInstance().getUid();
                                    String satu = dataViewHolder.Tanggal.getText().toString();
                                    String dua = dataViewHolder.Tempat.getText().toString();
                                    DatabaseReference r = FirebaseDatabase.getInstance().getReference("Jadwal").child(satu + dua);
                                    r.child("Respon").child(uid).child("jumlahh").removeValue();
                                    dataViewHolder.love.setActivated(false);
                                    dataViewHolder.love.setImageDrawable(ContextCompat.getDrawable(getContext(),R.drawable.ic_favorite_black_24dp));

                                }
                            });
                            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                }
                            });
                            AlertDialog alertDialog = builder.create();
                            alertDialog.show();

                        }

                    }
                });

            }

            @NonNull
            @Override
            public DataViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new DataViewHolder(LayoutInflater.from(getActivity()).inflate(R.layout.item_jadwal,parent,false));
            }
        };
        rec.setAdapter(firebaseRecyclerAdapter);



        return rootView;  // Inflate the layout for this fragment

    }
    public static long pushAppointmentsToCalender(Activity curActivity, String a, String b, String c, String d) {
        /***************** Event: note(without alert) *******************/
        String title = "kegitan donor darah";
        String addInfo = b;
        String place = d;
        ParsePosition parsePositiona = new ParsePosition(0);
        String status = "1";
        String dateString = a + " " + c;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.US);
        Date date = sdf.parse(dateString, parsePositiona);

        long startDate = date.getTime();
        String eventUriString = "content://com.android.calendar/events";
        boolean needReminder = true;

        ContentValues eventValues = new ContentValues();

        eventValues.put("calendar_id", 1); // id, We need to choose from
        // our mobile for primary
        // its 1
        eventValues.put("title", title);
        eventValues.put("description", addInfo);
        eventValues.put("eventLocation", place);

        long endDate = startDate + 1000 * 60 * 60; // For next 1hr

        eventValues.put("dtstart", startDate);
        eventValues.put("dtend", endDate);

        // values.put("allDay", 1); //If it is bithday alarm or such
        // kind (which should remind me for whole day) 0 for false, 1
        // for true
        eventValues.put("eventStatus", status); // This information is
        // sufficient for most
        // entries tentative (0),
        // confirmed (1) or canceled
        // (2):
        eventValues.put("eventTimezone", "UTC/GMT +7:00");
        /*Comment below visibility and transparency  column to avoid java.lang.IllegalArgumentException column visibility is invalid error */

    /*eventValues.put("visibility", 3); // visibility to default (0),
                                        // confidential (1), private
                                        // (2), or public (3):
    eventValues.put("transparency", 0); // You can control whether
                                        // an event consumes time
                                        // opaque (0) or transparent
                                        // (1).
      */
        eventValues.put("hasAlarm", 1); // 0 for false, 1 for true

        Uri eventUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(eventUriString), eventValues);
        long eventID = Long.parseLong(eventUri.getLastPathSegment());

        if (needReminder) {
            /***************** Event: Reminder(with alert) Adding reminder to event *******************/

            String reminderUriString = "content://com.android.calendar/reminders";

            ContentValues reminderValues = new ContentValues();

            reminderValues.put("event_id", eventID);
            reminderValues.put("minutes", 60);
            // Default value of the
            // system. Minutes is a
            // integer
            reminderValues.put("method", 1); // Alert Methods: Default(0),
            // Alert(1), Email(2),
            // SMS(3)

            Uri reminderUri = curActivity.getApplicationContext().getContentResolver().insert(Uri.parse(reminderUriString), reminderValues);
        }

        /***************** Event: Meeting(without alert) Adding Attendies to the meeting *******************/


        return eventID;
    }
    private boolean permissionAlreadyGranted() {

        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_CALENDAR);
        if (result == PackageManager.PERMISSION_GRANTED)
            return true;
        return false;
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_CALENDAR)) {
        }
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR}, CAMREA_CODE);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == CAMREA_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getActivity(), "Permission granted successfully", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Permission is denied!", Toast.LENGTH_SHORT).show();
                boolean showRationale = shouldShowRequestPermissionRationale( Manifest.permission.WRITE_CALENDAR );
                if (! showRationale) {
                    openSettingsDialog();
                }
            }
        }
    }
    private void openSettingsDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setPositiveButton("Take Me To SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }
}
