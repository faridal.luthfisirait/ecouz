package com.example.pmikotamedanskripsi;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.widget.TextViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;



public class DarahFragment extends Fragment {
    String saprc,sawb, sakarantina, sbprc, sbwb, sbkarantina, sabprc, sabwb, sabkarantina, soprc, sowb, sokarantina;
    TextView aprc, awb, akarantina, bprc, bwb, bkarantina, abprc, abwb, abkarantina, oprc, owb, okarantina;



    public DarahFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_darah, container, false);
        aprc = rootView.findViewById(R.id.aprc);
        awb = rootView.findViewById(R.id.awb);
        akarantina = rootView.findViewById(R.id.akaran);
        bprc = rootView.findViewById(R.id.bprc);
        bwb = rootView.findViewById(R.id.bwb);
        bkarantina = rootView.findViewById(R.id.bkaran);
        abprc = rootView.findViewById(R.id.abprc);
        abwb = rootView.findViewById(R.id.abwb);
        abkarantina = rootView.findViewById(R.id.abkaran);
        oprc = rootView.findViewById(R.id.oprc);
        owb = rootView.findViewById(R.id.owb);
        okarantina = rootView.findViewById(R.id.okaran);


        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("GolonganDarah");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                saprc =   dataSnapshot.child("A").child("PRC").getValue().toString();
                sawb =  dataSnapshot.child("A").child("WB").getValue().toString();
                sakarantina = dataSnapshot.child("A").child("Karantina").getValue().toString();
                sbprc =   dataSnapshot.child("B").child("PRC").getValue().toString();
                sbwb =  dataSnapshot.child("B").child("WB").getValue().toString();
                sbkarantina = dataSnapshot.child("B").child("Karantina").getValue().toString();
                sabprc =   dataSnapshot.child("AB").child("PRC").getValue().toString();
                sabwb =  dataSnapshot.child("AB").child("WB").getValue().toString();
                sabkarantina = dataSnapshot.child("AB").child("Karantina").getValue().toString();
                soprc =   dataSnapshot.child("O").child("PRC").getValue().toString();
                sowb =  dataSnapshot.child("O").child("WB").getValue().toString();
                sokarantina = dataSnapshot.child("O").child("Karantina").getValue().toString();



                aprc.setText(saprc);
                awb.setText(sawb);
                akarantina.setText(sakarantina);

                bprc.setText(sbprc);
                bwb.setText(sbwb);
                bkarantina.setText(sbkarantina);

                abprc.setText(sabprc);
                abwb.setText(sabwb);
                abkarantina.setText(sabkarantina);

                oprc.setText(soprc);
                owb.setText(sowb);
                okarantina.setText(sokarantina);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        return rootView;
    }

}
