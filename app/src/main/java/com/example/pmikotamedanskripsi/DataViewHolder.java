package com.example.pmikotamedanskripsi;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import androidx.recyclerview.widget.RecyclerView;

public class DataViewHolder extends RecyclerView.ViewHolder {
    TextView Tempat, Alamat, Tanggal, Waktu ;
    boolean cek;
    ImageView love;


    public DataViewHolder(@NonNull View itemView) {
        super(itemView);
        Tempat = itemView.findViewById(R.id.tempat1);
        Alamat = itemView.findViewById(R.id.alamat);
        Tanggal = itemView.findViewById(R.id.tanggal);
        Waktu = itemView.findViewById(R.id.waktuu);
        love = itemView.findViewById(R.id.love);

    }
}
