package com.example.pmikotamedanskripsi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

public class Verifikasi extends AppCompatActivity {
    String nohp1, mVerfikasi;
    FirebaseAuth mAuth;
    EditText otp, text;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verifikasi);
        mAuth =FirebaseAuth.getInstance();
        String nohp = getIntent().getStringExtra("noo");
        nohp1 = "+62"+nohp;
        otp =findViewById(R.id.otpp);
        text = findViewById(R.id.t);

        text.setText("Kami Telah Mengirimkan Kode OTP Ke No +62"+nohp);
        KirimOTP(nohp1);

        findViewById(R.id.verifikasi).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                findViewById(R.id.verifikasi).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String otpp =otp.getText().toString().trim();
                        if (otpp.isEmpty()){
                            otp.setHint("Tidak Boleh Kosong");
                        }
                        else {verif(otpp);}
                    }
                });



            }
        });
    }
    private void KirimOTP (String no){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(no,60, TimeUnit.SECONDS, TaskExecutors.MAIN_THREAD,mCallback );

    }
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallback = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            Masuk(phoneAuthCredential);
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {

        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);
            mVerfikasi =s;
        }
    };
    private void Masuk(PhoneAuthCredential credential){
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Intent intent = new Intent(Verifikasi.this,FormDataDiri.class);
                            intent.putExtra("nohp", nohp1);
                            startActivity(intent);
                            finish();
                        }
                        else {
                            otp.setHint("Kode Salah");
                        }

                    }
                });

    }
    private void verif(String code){
        try {
            PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerfikasi, code);
            Masuk(credential);
        }catch (Exception e){
            Toast toast = Toast.makeText(this,"Verifikasi Kode Salah", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
