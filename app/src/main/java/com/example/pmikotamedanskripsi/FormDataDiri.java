package com.example.pmikotamedanskripsi;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

public class FormDataDiri extends AppCompatActivity {
    AutoCompleteTextView showw;
DatabaseReference reference;
    EditText tgl, nama;
    String selection1, nohp;
    ImageView show1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_data_diri);
        showw = findViewById(R.id.acomponen);
        show1 = findViewById(R.id.acomponen2);
        nohp = getIntent().getStringExtra("nohp");
        tgl = findViewById(R.id.tgllahir);
        nama = findViewById(R.id.nama);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(FormDataDiri.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.goldarah));
        showw.setAdapter(arrayAdapter);
        showw.setCursorVisible(false);
        showw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View arg0) {
                showw.showDropDown();

            }
        });
        showw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                showw.showDropDown();
                selection1=(String) adapterView.getItemAtPosition(i);


            }
        });
        show1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showw.showDropDown();
            }
        });
        tgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar newCalendar = Calendar.getInstance();

                DatePickerDialog datePickerDialog = new DatePickerDialog(FormDataDiri.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
                        tgl.setText(dateFormatter.format(newDate.getTime()));
                    }

                },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.show();
            }
        });


        findViewById(R.id.simpan).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nama.getText().toString();
                String tanggal  = tgl.getText().toString();
                String GOL = showw.getText().toString();

                if(name.isEmpty()){
                    Toast.makeText(FormDataDiri.this,"Masukan Nama",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(tanggal.isEmpty()){
                    Toast.makeText(FormDataDiri.this, "Masukan Tanggal Lahir",Toast.LENGTH_SHORT).show();
                    return;
                }
                if (GOL.isEmpty()){
                    Toast.makeText(FormDataDiri.this, "Pilih Golngan Darah",Toast.LENGTH_SHORT).show();
                    return;

                }


                else {
                    simpan(name,tanggal,GOL,nohp);


                }
            }
        });
    }

    private void simpan (String nama, String tanggal, String darah, final String nohpp){

        reference = FirebaseDatabase.getInstance().getReference("Pengguna").child(nohpp);
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("Nama", nama );
        hashMap.put("Tanggal", tanggal);
        hashMap.put("Goldarah",darah);
        hashMap.put("No", nohpp);


        reference.setValue(hashMap).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
              Intent intent = new Intent(FormDataDiri.this, MenuUtama.class);
              intent.putExtra("nohp", nohp);
                startActivity(intent);
                finish();}
                else {Toast.makeText(FormDataDiri.this,"Jaringan Tidak Ada",Toast.LENGTH_SHORT).show();
                return;}

            }
        });
    }
}
