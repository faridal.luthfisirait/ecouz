package com.example.pmikotamedanskripsi;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    EditText phone;
    FirebaseUser firebaseUser;
    @Override
    protected void onStart() {
        super.onStart();
        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        if(firebaseUser !=null){
            Intent intent = new Intent(MainActivity.this,MenuUtama.class);
            startActivity(intent);
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        phone = findViewById(R.id.notelp);



        findViewById(R.id.lanjut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String no = phone.getText().toString().trim();
                Intent intent = new Intent(MainActivity.this, Verifikasi.class);
                intent.putExtra("noo", no);
                startActivity(intent);
                finish();
            }
        });
    }
}
