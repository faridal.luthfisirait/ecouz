package com.example.pmikotamedanskripsi;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DataFirebaseJadwal {

    String Tempat;
    String Alamat;
    String Tanggal;
    String Waktu;
    boolean a ;
    boolean cek = a ;


    public String getTempat() {
        return Tempat;
    }
    public String getAlamat() {
        return Alamat;
    }
    public String getTanggal() {
        return Tanggal;
    }
    public String getWaktu() {
        return Waktu;
    }
    public boolean  getCek() { return cek ;}

    public void setTempat(String Tempat) {
        this.Tempat = Tempat;
    }
    public void setAlamat(String Alamat) {
        this.Alamat = Alamat;
    }
    public void setTanggal(String Tanggal) {
        this.Tanggal = Tanggal;
    }
    public void setWaktu(String Waktu) {
        this.Waktu = Waktu;
    }
    public void setCek(boolean cek) {
        this.cek = cek;
    }


    public DataFirebaseJadwal() {
    }

    public DataFirebaseJadwal(String Tempat, String Alamat, String Tanggal, String Waktu, boolean cek) {

        this.Tempat = Tempat;
        this.Alamat = Alamat;
        this.Tanggal = Tanggal;
        this.Waktu = Waktu;
        this.cek= cek;
    }
}
